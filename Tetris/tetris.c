//
// Created by Aurélien VALADE on 18/06/17.
//
// Moteur de jeu Tetris pour STM32 s'appuyant sur une bibliothèque d'abstraction pour LCD ILI9341 320x240
//

#include <stdint.h>
#include <stdlib.h>
#include "tetris.h"
#include "keys.h"
#include <WS2813.h>
#include <time.h>


/**
 * Définitions locales
 */
#define GRID_WIDTH 10
#define GRID_HEIGHT 22
#define GRID_OFFSCREEN 2
#define BLOCK_WIDTH 11
#define BLOCK_HEIGHT 11
#define SCREEN_OFFSET_X 110
#define SCREEN_OFFSET_Y 8
#define GRID_BLANK_VALUE 8
#define GRID_ERASE_VALUE 7

// Transformation de coordonnées (X,Y) => numero de LED
#define TRANSFORM_COORD(x,y) ((19-(y))*10+((((y)&1)!=0)?(9-(x)):x))
#define TRANSFORM_NEXT_BLOCK(x,y) (200+(y)*4+((((y)&1)!=0)?(3-(x)):x))

// Tetrominos
typedef uint16_t TETROMINO;

typedef enum{
    TETRIS_PLAYING,
    TETRIS_GAMEOVER
}TETRIS_MODE;

/**
 * Variables locales
 */
static uint8_t __fullLines[4];
static uint8_t __gameSpeed = 25;
static uint8_t __speedCounter = 0;
static uint16_t __piecesCount = 0;
static uint16_t __score;
static uint16_t __lines = 0;
static uint8_t __nextTetromino = 0;
static void(*__score_callback)(uint32_t score);

// Tetrominos shapes definition
static const TETROMINO __tetrominos[] = {
        0b0010001000100010,  // I
        0b0000011001100000,  // O
        0b0110010001000000,  // L
        0b0110001000100000,  // J
        0b0010011001000000,  // Z
        0b0100011000100000,  // S
        0b0010011000100000,  // T
};

// Tetrominos color definition
static const WS2813_COLOR __tetrominoColors[] =
{
    (WS2813_COLOR){.rgb={.r=0, .g=127, .b=127}},     // I
    (WS2813_COLOR){.rgb={.r=127, .g=127, .b=0}},     // O
    (WS2813_COLOR){.rgb={.r=0, .g=0, .b=200}},       // L
	(WS2813_COLOR){.rgb={.r=150, .g=64, .b=0}},      // J
    (WS2813_COLOR){.rgb={.r=255, .g=0, .b=0}},       // Z
	(WS2813_COLOR){.rgb={.r=0, .g=255, .b=0}},       // S
	(WS2813_COLOR){.rgb={.r=127, .g=64, .b=64}},     // T
	(WS2813_COLOR){.rgb={.r=255, .g=255, .b=255}},   // Line completed
	(WS2813_COLOR){.rgb={.r=0, .g=0, .b=0}}  	    // Background color
};

// Game grid buffer
static uint8_t __gameGrid[GRID_HEIGHT*GRID_WIDTH];
// Current game mode
static TETRIS_MODE __gameMode;


/**
 * Fonctions privées
 */
static void __tetrisDrawGrid();
static uint8_t __tetrominoGetXY(uint8_t id, uint8_t rotation, uint8_t x, uint8_t y);



/**
 * @brief Draw the game grid
 *
 * This function refreshed the game grid
 */
static void __tetrisDrawGrid()
{
    int8_t x, y;
    char sScore[30];

    // For each element of the grid, draw the block color
    for (x=0;x<GRID_WIDTH;x++)
        for (y=GRID_OFFSCREEN;y<GRID_HEIGHT;y++)
        	ws2813_update_led(TRANSFORM_COORD(x, y-GRID_OFFSCREEN), __tetrominoColors[__gameGrid[x+(y*GRID_WIDTH)]]);

    // Display the game score
    /*LCD_set_foreground(ILI9341_BLACK);
    LCD_set_background(3167);
    LCD_set_font(&Font12);
    sprintf(sScore, "%d", __score);
    LCD_fill_rect(240, 150, 80, 80, 3167);
    LCD_print(240, 150, "Score :");
    LCD_print(240, 162, sScore);

    // And completed lines count
    LCD_print(240, 180, "Line :");
    sprintf(sScore, "%d", __lines);
    LCD_print(240, 192, sScore);
     */
    // And display the next tetromino to fall

    // TODO : Next to fall
    for (x=0;x<4;x++)
        for (y=0;y<4;y++)
            if (__tetrominoGetXY(__nextTetromino, 0, x, y))
                ws2813_update_led(TRANSFORM_NEXT_BLOCK(x,y), __tetrominoColors[__nextTetromino]);
            else
            	ws2813_update_led(TRANSFORM_NEXT_BLOCK(x,y), __tetrominoColors[GRID_BLANK_VALUE]);

}


/**
* Functions
*/


/**
 * @brief Game initialization
 *
 * Initialize the game variables and the game display
 */
void tetrisInit()
{
    uint16_t i=0;

    // Display the game grid background
    for (i=0;i<GRID_HEIGHT*GRID_WIDTH;i++)
        __gameGrid[i]=GRID_BLANK_VALUE;

    // Set the game mode as Playing
    __gameMode = TETRIS_PLAYING;

    // Initialize the game variables
    __score=0;
    __piecesCount = 0;
    __gameSpeed = 25;
    __lines = 0;
    __nextTetromino = rand() % 7;
    __tetrisDrawGrid();

    __score_callback = NULL;

    // Seed the game
    srand(time(0));
}

/**
 * @brief Get a tetromino block occupancy base on it's id, rotation and coordinates
 *
 * @param id tetromino identifier
 * @param rotation tetromino rotation
 * @param x x position inside the tetromino
 * @param y y position inside the tetromino
 *
 * @return Block occupancy status
 */
uint8_t __tetrominoGetXY(uint8_t id, uint8_t rotation, uint8_t x, uint8_t y)
{
    uint8_t index;
    if (id>=7)
        return 0;

    // Compute the position of the bit based on the rotation
    switch (rotation%4)
    {
        case 0:
            index = x+(y*4);
            break;
        case 1:
            index = 12 + y - x*4;
            break;
        case 2:
            index = 15 - (x + y*4);
            break;
        case 3:
            index = 3 - y + (x*4);
            break;
    }

    // And return the occupancy
    return (__tetrominos[id] & (0x8000>>index)) != 0;
}


/**
 * @brief Draw a single Tetromino into the game grid
 *
 * This function is used to draw falling tetrominos
 *
 * @param id Tetromino identifier
 * @param rotation Tetromino rotation
 * @param x Tetromino X position (top left corner)
 * @param y Tetromino Y position (top left corner)
 * @param erase erase or draw switch (1 : fill with background color, 0 : fill with specific color)
 */
void tetrisDrawTetromino(uint8_t id, uint8_t rotation, int8_t x, int8_t y, uint8_t erase)
{
    int i,j;

    // For each block of the tetromino
    for (i=0;i<4;i++)
        for (j=0;j<4; j++)
        {
        	// If the block is to be displayed
            if (__tetrominoGetXY(id, rotation, i, j) != 0 && (j+y)>=GRID_OFFSCREEN)
            {
            	// Fill the block with the color
                /*LCD_fill_rect(SCREEN_OFFSET_X + (x+i)*BLOCK_WIDTH,
                              SCREEN_OFFSET_Y+ (y+j-GRID_OFFSCREEN)*BLOCK_HEIGHT,
                              BLOCK_WIDTH, BLOCK_HEIGHT,
                              erase?__tetrominoColors[GRID_BLANK_VALUE]:__tetrominoColors[id] );*/
            	ws2813_update_led(TRANSFORM_COORD((x+i), (y+j-GRID_OFFSCREEN)),  erase?__tetrominoColors[GRID_BLANK_VALUE]:__tetrominoColors[id]);
            }
        }
}


/**
 * @brief Check for tetromino collision with the environment
 *
 * @param id Tetromino identifier
 * @param rotation Tetromino rotation
 * @param x top left corner x position
 * @param y top left corner y position
 * @return collisiion status ( 1 if collided with some obstacle, 0 if no collision)
 */
uint8_t tetrominoCheckCollision(uint8_t id, uint8_t rotation, int8_t x, int8_t y)
{
    int8_t i,j;
    if (id>=7)
        return 1;

    // For each block in the tetromino
    for (i=0;i<4;i++)
        for (j=0;j<4;j++)
        {
        	// If the block is occupied
            if (__tetrominoGetXY(id, rotation, i, j))
            {
            	// Check collition with borders
                if (x+i<0 || (x+i)>=GRID_WIDTH)
                    return 1;
                // Bottom
                if ((j+y) >= GRID_HEIGHT)
                    return 1;
                // Or grid occupancy
                if (__gameGrid[(i+x)+GRID_WIDTH*(j+y)] != GRID_BLANK_VALUE)
                    return 1;
            }
        }

    return 0;
}

void tetrisSetScoreCallback(void(*callback)(uint32_t score))
{
	__score_callback = callback;
}

/**
 * @brief Push a tetromino to the grid
 *
 * When a tetromino stops falling, this function pushes it to the static game grid
 *
 * @param id Tetromino identifier
 * @param rotation Tetromino rotation
 * @param x top left corner x position
 * @param y top left corner y position
 */
void pushTetromino(uint8_t id, uint8_t rotation, int8_t x, int8_t y)
{
    int8_t i,j;
    if (id>=7)
        return;

    // For each block of the tetromino
    for (i=0;i<4;i++)
        for (j=0;j<4;j++)
        {
        	// If the block is occupied
            if (__tetrominoGetXY(id, rotation, i, j))
            	// Pushes the value to the game grid
                __gameGrid[(x+i)+((y+j)*GRID_WIDTH)] = id;
        }
}

/**
 * @brief Check for a full line completion in around the last fallen tetromino
 *
 * This function only checks complete lines around the 4 lines below the top left corner
 * of the last fallen tetromino (because only these lines could have been completed during
 * this round).
 *
 * @param last_y top left corner Y position of the last tetromino
 */
void tetrisCheckFullLine(uint8_t last_y)
{
    uint8_t x,y;

    // For the next 4 lines
    for (y=0;y<4;y++)
    {
    	// Tells the line is full
        __fullLines[y] = 1;

        // If below the floor, can't be full
        if (y+last_y>=GRID_HEIGHT) {
            __fullLines[y]=0;
            break;
        }
        // Else
        for (x=0;x<GRID_WIDTH;x++)
        {
        	// If any block is background,
            if (__gameGrid[x + ((y+last_y)*GRID_WIDTH)]==GRID_BLANK_VALUE)
            	// Then the line is not full
                __fullLines[y] = 0;
        }
    }
}

/**
 * @brief Game update
 *
 * This function handles the whole game animation an gameplay mechanics.
 *
 * @param active_keys keys states
 * @param click_keys click detection for keys
 * @return Game status (-1 for game finished)
 */
int tetrisUpdate(KEYS active_keys, KEYS click_keys)
{
	// Behavior depends on game mode
    switch (__gameMode)
    {
    	// In playing mode
        case TETRIS_PLAYING:
        {
        	// Static variables containing tetromino info
            static int8_t x = 4, y=1;
            static uint8_t rotation = 0, id=9;
            uint8_t forced_move=0, pushback = 0;

            // If the counter reached the next move time
            if (++__speedCounter == __gameSpeed)
            {
            	// Force the move
                forced_move=1;
                __speedCounter=0;
            }

            // Force random number if id is greater than max tetromino ID
            if (id>=7)
                id=rand()%7;

            // If tetromino rotation is asked
            if (click_keys.up)
            {
                // TODO: Handle Wall Kick
            	// Clear the current tetromino
                tetrisDrawTetromino(id, rotation, x, y, 1);
                // And check if the rotation creates a collision
                if (!tetrominoCheckCollision(id, rotation+1, x, y))
                	// If ok, rotate the tetromino
                    rotation++;
            }

            // If moving left is asked
            if (click_keys.back)
            {
            	// Clear the current tetromino
                tetrisDrawTetromino(id, rotation, x, y, 1);
                // Check if the move is allowed
                if (!tetrominoCheckCollision(id, rotation, x-1, y))
                	// And do it
                    x--;
            }

            // If moving right is asked
            if (click_keys.enter)
            {
            	// Clear the current tetromino
                tetrisDrawTetromino(id, rotation, x, y, 1);
                // Check if the move is allowed
                if (!tetrominoCheckCollision(id, rotation, x+1, y))
                	// And do it
                    x++;
            }

            // If falling is asked
            if (active_keys.down || forced_move)
            {
            	// Erase the tetromino
                tetrisDrawTetromino(id, rotation, x, y, 1);
                // Check if move is allowed
                if (!tetrominoCheckCollision(id, rotation, x, y+1))
                	// If so, to the move
                    y++;
                // Else, pushes the tetromino to the grid
                else pushback=1;
            }

            // If Tetromino is to be pushed
            if (pushback)
            {
                uint8_t erased = 0;

                // Pushes the tetromino to the grid
                pushTetromino(id, rotation, x, y);

                // Increase the score
                __score +=10;

                // Check for full lines
                tetrisCheckFullLine(y);

                uint8_t i;
                for (i=0;i<4;i++)
                {
                	// Erases full lines
                    if (__fullLines[i] == 1  && i+y<GRID_HEIGHT)
                    {
                        uint8_t j;
                        for (j=0;j<GRID_WIDTH;j++)
                        {
                            __gameGrid[j+((i+y)*GRID_WIDTH)] = GRID_ERASE_VALUE;
                        }
                        erased++;
                    }
                }

                // Increase score with the completed lines
                __score += 100*erased*erased;

                if (__score_callback != NULL)
                	__score_callback(__score);

                // Drop down the grid
                if(erased)
                {
                	// Refresh the display
                    __tetrisDrawGrid();

                    // For each full lines
                    for (i=0;i<4;i++)
                    {
                        if (__fullLines[i])
                        {
                        	// Replace the line with top elements recusively
                            __lines++;
                            uint8_t j,k;
                            for (k=i+y;k>0;k--)
                                for (j=0;j<GRID_WIDTH;j++)
                                {
                                    __gameGrid[j+((k)*GRID_WIDTH)] = __gameGrid[j+((k-1)*GRID_WIDTH)];
                                }
                            for (j=0;j<GRID_WIDTH;j++)
                            {
                                __gameGrid[j] = GRID_BLANK_VALUE;
                            }
                        }
                    }
                    // Wait 500ms for the drop animation
                    HAL_Delay(500);
                }

                // Reinitialize the new tetromino
                x=4;y=1;
                rotation = 0;
                id=__nextTetromino;
                __nextTetromino=rand()%7;

                // Increase speed every 10 pieces
                if ((++__piecesCount)%10 == 0){
                    __gameSpeed--;
                }

                // Refresh the screen
                __tetrisDrawGrid();

                // Handling Game Over
                if (tetrominoCheckCollision(id, rotation, x, y))
                {
                 /*   LCD_set_address_window(0, 0, 320, 240);
                    LCD_write_pixels(backgroundTetris, 320*240);
                    LCD_set_font(&Font24);
                    LCD_fill_rect(50, 40, 220, 100, 3167);
                    LCD_print(85, 45, "GAME OVER");
                    LCD_print(100, 70, "Score");
                    char sScore[25];
                    snprintf(sScore, 25, "%d", __score);
                    LCD_print(105, 100, sScore);*/
                    __gameMode = TETRIS_GAMEOVER;
                }


            }

            // Draw new tetromino
            tetrisDrawTetromino(id, rotation, x, y, 0);

            ws2813_start_transmission_tim();

            break;
        }


        // In game over mode
        case TETRIS_GAMEOVER:
        	return -1;
            break;
    }

    return 0;

}
