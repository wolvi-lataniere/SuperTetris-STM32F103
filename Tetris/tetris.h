//
// Created by Aurélien VALADE on 18/06/17.
//
// Moteur de jeu Tetris pour STM32 s'appuyant sur une bibliothèque d'abstraction pour LCD ILI9341 320x240
//

#ifndef SDLTRIS_TETRIS_H
#define SDLTRIS_TETRIS_H

#include "keys.h"

#ifdef __cplusplus
extern "C"{
#endif


void tetrisInit();
int tetrisUpdate(KEYS active_keys, KEYS click_keys);
void tetrisSetScoreCallback(void(*callback)(uint32_t score));



#ifdef __cplusplus
};
#endif

#endif //SDLTRIS_TETRIS_H
