//
// Created by wolverine on 18/06/17.
//

#ifndef SDLTRIS_KEYS_H
#define SDLTRIS_KEYS_H

#include <stdint.h>

typedef struct{
    uint8_t up:1;
    uint8_t down:1;
    uint8_t back:1;
    uint8_t enter:1;
    uint8_t space:1;
}KEYS;


#endif //SDLTRIS_KEYS_H
