/*
 * esp8266.h
 *
 * Pilote pour module WiFi ESP8266
 *
 *  Created on: 17 janv. 2018
 *      Author: Aurélien VALADE
 */

#ifndef ESP8266_H_
#define ESP8266_H_

#include "usart.h"

#define DEFAULT_SSID "Tetris"
#define DEFAULT_PASS "SuperTetris"

typedef enum{
	CH_CONNECT,
	CH_DISCONNECT
} CHAN_STATUS;

typedef void(*esp8266_reception_callback)(uint8_t channel, uint8_t *data, uint16_t len);
typedef void(*esp8266_conn_status_callback)(uint8_t channel, CHAN_STATUS status);
typedef enum{
	ESP_OK,
	ESP_FAILED
} ESP8266_STATUS;


void esp8266_init_driver(USART_TypeDef *huart, DMA_TypeDef* dma, uint32_t channel);
ESP8266_STATUS esp8266_connect(char* ssid, char* pass);
ESP8266_STATUS esp8266_open(char* host, uint16_t port, uint8_t channel, esp8266_reception_callback reception_callback);
ESP8266_STATUS esp8266_send_data(uint8_t channel, uint8_t* buffer, uint16_t len);
ESP8266_STATUS esp8266_create_ap(char* ssid, char* pass);
ESP8266_STATUS esp8266_AP_set_ip(char* address);
ESP8266_STATUS esp8266_server_create(uint16_t port, esp8266_conn_status_callback callback);
ESP8266_STATUS esp8266_register_callback(uint8_t channel, esp8266_reception_callback callback);
ESP8266_STATUS esp8266_server_set_timeout(uint16_t timeout);

void esp8266_usart_tx_complete();
void esp8266_usart_rx_handler();

#endif /* ESP8266_H_ */
