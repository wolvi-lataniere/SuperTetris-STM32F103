/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "WS2813.h"
#include "main.h"
#include <tim.h>
#include <spi.h>
#include <tetris.h>
#include <usart.h>
#include <esp8266.h>
#include <string.h>
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */
int8_t channel;
char score_buffer[50];

void score_callback(uint32_t score);
void send_wifi(char* data);
void reception_callback(uint8_t channel, uint8_t *data, uint16_t len);
void connection_status(uint8_t ch, CHAN_STATUS status);

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void vAnimationTimer(TimerHandle_t timer);
char buffer[250];
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  channel = -1;
  ws2813_init_tim(&htim2, TIM_CHANNEL_1, 216);
  tetrisInit();
  tetrisSetScoreCallback(score_callback);

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  TimerHandle_t timer = xTimerCreate("animation",
		  pdMS_TO_TICKS(20),
		  pdTRUE,
		  NULL,
		  vAnimationTimer);

  xTimerStart(timer, pdMS_TO_TICKS(100));
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityIdle, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  esp8266_init_driver(USART2, DMA1, LL_DMA_CHANNEL_7);
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  while (esp8266_AP_set_ip("192.168.50.1") == ESP_FAILED);
  esp8266_create_ap("SuperTetris", "MegaTetris");
  esp8266_server_create(3000, connection_status);
  esp8266_server_set_timeout(120);

  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */
void send_wifi(char* data)
{
	if (channel >= 0)
		esp8266_send_data(channel, (uint8_t*)data, strlen(data));
}

void score_callback(uint32_t score){
	snprintf(score_buffer, 50, "{'score':%d}", score);
	send_wifi(score_buffer);
}

void reception_callback(uint8_t channel, uint8_t *data, uint16_t len)
{
	send_wifi("{'pong':1}");
}

void connection_status(uint8_t ch, CHAN_STATUS status)
{
	if (status == CH_CONNECT)
	{
		channel = ch;
		esp8266_register_callback(ch, reception_callback);
	}
	else
	{
		channel = -1;
		esp8266_register_callback(ch, NULL);
	}
}


void vAnimationTimer(TimerHandle_t timer)
{
	static int game_status = 0;
	KEYS k, click;
	static KEYS prev;

	uint8_t data;
	HAL_GPIO_WritePin(JS_SELECT_GPIO_Port, JS_SELECT_Pin, GPIO_PIN_RESET);
	vTaskDelay(1);
	HAL_GPIO_WritePin(JS_SELECT_GPIO_Port, JS_SELECT_Pin, GPIO_PIN_SET);

	HAL_SPI_Receive(&hspi1, &data, 1, 100);

	k.down = (data & (1<<3))==0;
	k.up = (((data & (1<<5))==0) || ((data & (1<<1))==0) ) ;
	k.enter = (data & (1<<2))==0;
	k.back = (data & (1<<4))==0;

	click.down = k.down & !prev.down;
	click.up = k.up & !prev.up;
	click.back = k.back & !prev.back;
	click.enter = k.enter & !prev.enter;
	switch(game_status)
	{
	case 1:
		if (tetrisUpdate(k,click) == -1)
		{
			game_status = 0;
			if (channel >= 0)
				send_wifi("{'status':'game_over'}");
		}
		break;

	case 0:
		if (click.up)
		{
			if (channel >= 0)
				send_wifi("{'status':'new_game'}");
			game_status=1;
			tetrisInit();
			tetrisSetScoreCallback(score_callback);
		}
	}

	prev=k;
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
