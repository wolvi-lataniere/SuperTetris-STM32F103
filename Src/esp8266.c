/*
 * esp8266.c
 *
 * Pilote pour module WiFi ESP8266
 *
 *  Created on: 17 janv. 2018
 *      Author: Aurélien VALADE
 */

#include <esp8266.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include "cmsis_os.h"

#define CHANNELS_COUNT 5
#define RECEPTION_BUFFER_LEN 50

/////// Local types definition ////////
typedef struct{
	uint8_t* data;
	uint16_t size;
}UART_MESSAGE;

typedef struct{
	uint8_t channel;
	esp8266_reception_callback callback;
}UART_CHANNEL_CALLBACK;


typedef enum{
	RX_IDLE,
	RX_COMMAND,
	RX_DATA
} RX_STATE;

typedef enum{
	RXDS_IPD,
	RXDS_ID,
	RXDS_LEN,
	RXDS_DATA,
	RXDS_OTHER
} RX_DATA_STATE;


/////// Private variables ////////
static USART_TypeDef *__huart;
static DMA_TypeDef* __dma;
static uint32_t __dma_channel;
static uint8_t __uart_echo=0;
static QueueHandle_t *__messages_to_send;
static SemaphoreHandle_t *__reception_semaphore;
static SemaphoreHandle_t *__transmission_semaphore;
static SemaphoreHandle_t *__data_reception_semaphore;

static esp8266_conn_status_callback __status_callback;
static esp8266_reception_callback __uart_callbacks[CHANNELS_COUNT];
static char __transmission_buffer[255];
static char __reception_buffer[RECEPTION_BUFFER_LEN];
static uint16_t __reception_buffer_position;
static uint8_t __current_channel;

static RX_STATE __rx_state;
static RX_DATA_STATE __rxd_state;
static uint16_t __rxd_len;

/////// Private functions prototypes ////////
void __esp8266_task(void* param);
void __esp8266_write_data(uint8_t* data, uint16_t size);
ESP8266_STATUS esp8266_send_command(char *command);

/////// Public functions ////////
void esp8266_init_driver(USART_TypeDef *huart, DMA_TypeDef* dma, uint32_t channel)
{
	// Copy the uart handler
	__huart = huart;
	__dma = dma;
	__dma_channel = channel;

	__status_callback = NULL;


	for (__current_channel=0;__current_channel < CHANNELS_COUNT; __current_channel++)
	{
		__uart_callbacks[__current_channel] = NULL;
	}

	__current_channel = 0;
	__rx_state = RX_IDLE;

	__reception_buffer_position = 0;

	// Create the messages queue
	__messages_to_send = xQueueCreate(3, sizeof(UART_MESSAGE));
	__reception_semaphore = xSemaphoreCreateBinary();
	__transmission_semaphore = xSemaphoreCreateBinary();
	__data_reception_semaphore = xSemaphoreCreateBinary();
	xSemaphoreGive(__transmission_semaphore);

	LL_USART_EnableIT_RXNE(__huart);
	LL_USART_EnableIT_PE(__huart);
	LL_USART_EnableIT_ERROR(__huart);
	LL_USART_DisableIT_TXE(__huart);
	LL_USART_DisableIT_TC(__huart);

	// Create the task
	xTaskCreate(__esp8266_task,
			    "ESP8266",
				400,
				NULL,
				osPriorityLow,
				NULL);
}

ESP8266_STATUS esp8266_connect(char* ssid, char* pass)
{
	return ESP_FAILED;
}

ESP8266_STATUS esp8266_create_ap(char* ssid, char* pass)
{
	sprintf(__transmission_buffer, "AT+CWSAP=\"%s\",\"%s\",10,3\r\n", ssid, pass);
	esp8266_send_command(__transmission_buffer);
	esp8266_send_command("AT+CWMODE=2\n");

	return esp8266_send_command("AT+CWDHCP=0,1\r\n");
}

ESP8266_STATUS esp8266_AP_set_ip(char* address)
{
	sprintf(__transmission_buffer, "AT+CIPAP=\"%s\"\r\n", address);
	return esp8266_send_command(__transmission_buffer);
}

ESP8266_STATUS esp8266_open(char* host, uint16_t port, uint8_t channel, esp8266_reception_callback reception_callback)
{
	return ESP_FAILED;
}

ESP8266_STATUS esp8266_send_data(uint8_t channel, uint8_t* buffer, uint16_t len)
{
	sprintf(__transmission_buffer, "AT+CIPSEND=%d,%d\r\n", channel, len);
	esp8266_send_command(__transmission_buffer);
	__esp8266_write_data(buffer, len);
	return ESP_OK;//esp8266_send_command("AT\r\n");
}

ESP8266_STATUS esp8266_send_command(char *command)
{
	while (__rx_state != RX_IDLE)
		vTaskDelay(1);
	//if (xSemaphoreTake(__transmission_semaphore, 10) == pdTRUE)
	{
		__rx_state = RX_COMMAND;
		__uart_echo+=2;
		__esp8266_write_data((uint8_t*)command, strlen(command));
		while (xSemaphoreTake(__reception_semaphore, 10) != pdTRUE)
			vTaskDelay(pdMS_TO_TICKS(10));

		__rx_state = RX_IDLE;
		if (strncmp(__reception_buffer, "OK", 2) == 0)
			return ESP_OK;
	}
	return ESP_FAILED;
}

/////// Private functions ////////

/**
 * @brief ESP8266 Driver task
 *
 * This tasks handles the asynchronous transmission and reception of data
 */
void __esp8266_task(void* param)
{
	__uart_echo=0;
	esp8266_send_command("\rAT+RST\r\n");
	xSemaphoreTake(__transmission_semaphore, 100);
	vTaskDelay(pdMS_TO_TICKS(2000));
	xSemaphoreTake(__reception_semaphore, 1000);
	__reception_buffer_position = 0;
	__uart_echo=0;
	xSemaphoreGive(__transmission_semaphore);

	while(1)
	{
		if (xSemaphoreTake(__data_reception_semaphore, 1000) == pdTRUE)
		{
			if (strncmp(__reception_buffer, "IPD,", 4) == 0)
			{
				int id,len;
				int pos = 0;
				sscanf(__reception_buffer+4, "%d,%d:", &id, &len);

				while (__reception_buffer[pos++] != ':' && pos<RECEPTION_BUFFER_LEN);

				if (pos<RECEPTION_BUFFER_LEN && id >= 0 && id < CHANNELS_COUNT && __uart_callbacks[id] != NULL)
					__uart_callbacks[id](id, (uint8_t*)(__reception_buffer + pos), len);
			}
			else if (strncmp(__reception_buffer+1, ",CONNECT", 8) == 0 && __status_callback != NULL)
			{
				int id = __reception_buffer[0] - '0';
				__status_callback(id, CH_CONNECT);
			}
			else if (strncmp(__reception_buffer+1, ",CLOSED", 7) == 0 && __status_callback != NULL)
			{
				int id = __reception_buffer[0] - '0';
				__status_callback(id, CH_DISCONNECT);
			}
		}
	}
}


void __esp8266_write_data(uint8_t* data, uint16_t size)
{
	while (xSemaphoreTake(__transmission_semaphore, 1) == pdFALSE);
	__reception_buffer_position=0;
	LL_USART_EnableDMAReq_TX(__huart);
	LL_DMA_DisableChannel(__dma, __dma_channel);
	LL_DMA_SetMemoryAddress(__dma, __dma_channel, (uint32_t)data);
	LL_DMA_SetDataLength(__dma, __dma_channel, size);
	LL_DMA_SetMemoryIncMode(__dma, __dma_channel, LL_DMA_MEMORY_INCREMENT);
	LL_DMA_SetPeriphAddress(__dma, __dma_channel, LL_USART_DMA_GetRegAddr(__huart));
	LL_DMA_EnableIT_TC(__dma, __dma_channel);
	LL_DMA_EnableIT_TE(__dma, __dma_channel);
	LL_DMA_EnableChannel(__dma, __dma_channel);
}

ESP8266_STATUS esp8266_server_create(uint16_t port, esp8266_conn_status_callback callback)
{
	esp8266_send_command("AT+CIPMUX=1\r\n");
	sprintf(__transmission_buffer, "AT+CIPSERVER=1,%d\r\n", port);
	__status_callback = callback;
	return esp8266_send_command(__transmission_buffer);
}

ESP8266_STATUS esp8266_server_set_timeout(uint16_t timeout)
{
	sprintf(__transmission_buffer, "AT+CIPSTO=%d\r\n", timeout);
	return esp8266_send_command(__transmission_buffer);
}

ESP8266_STATUS esp8266_register_callback(uint8_t channel, esp8266_reception_callback callback)
{
	if (channel < CHANNELS_COUNT)
	{
		__uart_callbacks[channel] = callback;
		return ESP_OK;
	}

	return ESP_FAILED;
}

/////// UART Interrupts handling functions ////////
void esp8266_usart_tx_complete()
{
	LL_DMA_DisableChannel(__dma, __dma_channel);
	LL_USART_DisableDMAReq_TX(__huart);
	xSemaphoreGiveFromISR(__transmission_semaphore, NULL);
}

void esp8266_usart_rx_handler()
{
	char value = LL_USART_ReceiveData8(__huart);

	switch (__rx_state)
	{
	case RX_IDLE:
		if (value == '+')
		{
			__rx_state = RX_DATA;
			__rxd_state = RXDS_IPD;
			__rxd_len = 0;
			__reception_buffer_position = 0;
		}
		else if (value >= '0' && value <= '9')
		{
			__reception_buffer_position = 0;
			 __reception_buffer[__reception_buffer_position++] = value;
			 __rx_state = RX_DATA;
			 __rxd_state = RXDS_OTHER;
		}
		break;

	case RX_DATA:
		switch (__rxd_state)
		{
			case RXDS_IPD:
				if (value == ',')
					__rxd_state = RXDS_ID;
				break;
			case RXDS_ID:
				if (value == ',')
				{
					__rxd_len = 0;
					__rxd_state = RXDS_LEN;
				}
				break;
			case RXDS_LEN:
				if (value == ':')
					__rxd_state = RXDS_DATA;
				else{
					__rxd_len = (__rxd_len*10)+(value-'0');
				}
				break;
			case RXDS_DATA:
				if (--__rxd_len == 0)
				{
					__rx_state = RX_IDLE;
					xSemaphoreGiveFromISR(__data_reception_semaphore, NULL);
				}
				break;
			case RXDS_OTHER:
				if (value == '\n')
				{
					__rx_state = RX_IDLE;
					xSemaphoreGiveFromISR(__data_reception_semaphore, NULL);
				}
		}

		if (__reception_buffer_position < RECEPTION_BUFFER_LEN)
			__reception_buffer[__reception_buffer_position++] = value;

		break;

	case RX_COMMAND:
		if (value == '\n')
		{
			__reception_buffer_position = 0;
			if (__uart_echo==0)
			{
				xSemaphoreGiveFromISR(__reception_semaphore, NULL);
			}
			else __uart_echo--;
		}
		else if (value == '>')
		{
			xSemaphoreGiveFromISR(__reception_semaphore, NULL);
		}
		else
		{
			if (__reception_buffer_position < RECEPTION_BUFFER_LEN)
				__reception_buffer[__reception_buffer_position++] = value;
		}
	}
}
